#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "driver/uart.h"
#include "string.h"
#include "driver/gpio.h"

#include "swd.h"

void app_main(void) {

	swd_ll_init(10000, GPIO_NUM_33, GPIO_NUM_25, CONFIG_STM32_NRST);

	swd_connect();
//	swd_do_stuff();
}
