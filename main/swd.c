#include "swd_ll.h"
#include "swd.h"

enum access {
	dp = 0,
	ap,
};

unsigned swd_readl(enum access acc, unsigned addr, unsigned *val) {
	return swd_ll_readl(&swd, SWD_HEADER(acc, 1, addr), val);
}

unsigned swd_writel(enum access acc, unsigned addr, unsigned val) {
	return swd_ll_writel(&swd, SWD_HEADER(acc, 0, addr), val);
}

unsigned swd_readbuff(unsigned *val) {
	return swd_ll_readl(&swd, SWD_RDBUFF, val);
}

void write_select(unsigned ap, unsigned bank) {
	unsigned val = (bank & 0xf) << 4;
	val |= (ap & 0xff) << 24;
	swd_ll_writel(&swd, SWD_SELECT, val);
}

int dap_read(unsigned addr, unsigned *val) {
	unsigned rq_addr = (addr & 0xc) >> 2;
	swd_writel(dp, 2, addr & 0xf);
	return swd_readl(ap, rq_addr, val);
}

int dap_write(unsigned addr, unsigned val) {
	unsigned rq_addr = (addr & 0xc) >> 2;
	swd_writel(dp, 2, addr & 0xf);
	return swd_writel(ap, rq_addr, val);
}

int ap_read(unsigned addr, unsigned *val) {
	swd_ll_writel(&swd, SWD_AP_ADDR_WR, addr);
	swd_ll_readl(&swd, SWD_AP_DATA_RD, &val);
	return swd_readbuff(&val);
}

int ap_write(unsigned addr, unsigned val) {
	swd_ll_writel(&swd, SWD_AP_ADDR_WR, addr);
	swd_ll_writel(&swd, SWD_AP_DATA_WR, val);
	return swd_readbuff(&val);
}

void swd_connect(void) {
	unsigned id;
	unsigned val32;
	int ret;

	swd_ll_init_seq(&swd);

	/* READ ID */
	ret = swd_ll_readl(&swd, SWD_READ_ID, &id);
	if (ret) {
		printf("%s: ack: %d\n", __func__, ret);
	}
	printf("DP_ID: %08x\n", id);

	/* clear errors */
	val32 = 0xf << 1;
	swd_ll_writel(&swd, SWD_ABORT, val32);
	swd_ll_readl(&swd, SWD_AP_CTRL_RD, &val32);

	write_select(0, 0);

	val32 = 0xf << 1;
	swd_ll_writel(&swd, SWD_ABORT, val32);

	swd_ll_readl(&swd, SWD_DP_CTRL_RD, &val32);
	val32 = (1 << 28) | (1 << 30) | (1 << 5);
	swd_ll_writel(&swd, SWD_DP_CTRL_WR, val32);
	swd_ll_readl(&swd, SWD_DP_CTRL_RD, &val32);
	printf("SWD_DP_CTRL_RD: %08x\n", val32);
	
	/* wait untill power domains are activated */
	while (!(val32 & (1 << 29)))
		swd_ll_readl(&swd, SWD_DP_CTRL_RD, &val32);
	printf("CDBGPWRUPACK\n");

	while (!(val32 & (1 << 31)))
		swd_ll_readl(&swd, SWD_DP_CTRL_RD, &val32);
	printf("CSYSPWRUPACK\n");

	/* activat overrun checking */
	val32 = (1 << 30) | (1 << 28) | (1 << 26) | (1 << 5);
	swd_ll_writel(&swd, SWD_DP_CTRL_WR, val32);

	swd_ll_readl(&swd, SWD_DP_CTRL_RD, &val32);
	printf("SWD_DP_CTRL_RD: %08x\n", val32);

	swd_ll_readl(&swd, SWD_AP_CTRL_RD, &val32);
	val32 &= ~0xff;
	val32 |= (1 << 6) | (1 << 4) | 2;
	swd_ll_writel(&swd, SWD_AP_CTRL_WR, val32);
	swd_ll_readl(&swd, SWD_AP_CTRL_RD, &val32);
	printf("CSW: %08x\n", val32);

	swd_ll_writel(&swd, SWD_SELECT, 0x0);
	swd_ll_writel(&swd, SWD_AP_ADDR_WR, 0xc);

	write_select(0x0, 0xf);

	ret = swd_ll_readl(&swd, SWD_AP_DATA_RD, &val32);
	if (ret)
		printf("%s: ack: %d\n", __func__, ret);
	swd_readbuff(&val32);
	printf("AP_ID: %08x\n", val32);

	ret = swd_ll_readl(&swd, SWD_HEADER(1, 1, 2), &val32);
	swd_readbuff(&val32);
	printf("DEBUG_ROM: %08x\n", val32);

	write_select(0x0, 0x0);

	ap_read(0xe0042000, &val32);
	printf("CORE_ID: %08x\n", val32);

swd_ll_readl(&swd, SWD_DP_CTRL_RD, &val32);
printf(" STS:%08x\n", val32);


	ap_write(0xE000EDF0, 0xA05F0003);

	for (int i = 0; i < 0x20; i++) {
		unsigned address = 0x80000000 + (i << 2);
		ap_read(address, &val32);
		printf("%08x: %08x\n", address, val32);
	}

//	for (int z = 0; z < 0xff; z+=4) {
//		swd_ll_writel(&swd, SWD_AP_ADDR_WR, z);
//		ret = swd_ll_readl(&swd, SWD_AP_DATA_RD, &val32);
//		printf("%02x: %08x\n", z, val32);
//	}

}

void swd_do_stuff(void) {
	unsigned val;
	int ret;

	swd_ll_writel(&swd, SWD_SELECT, 0x0);
	swd_ll_readl(&swd, SWD_DP_CTRL_RD, &val);
	val = (1 << 28) | (1 << 30);
	swd_ll_writel(&swd, SWD_DP_CTRL_WR, val); 
	val = (1 << 31) | (1 << 2);
	swd_ll_readl(&swd, SWD_DP_CTRL_RD, &val);
	swd_ll_writel(&swd, SWD_AP_CTRL_WR, val);

	ret = swd_ll_readl(&swd, SWD_AP_CTRL_RD, &val);
	val |= 2 | (1 << 6) | (1 << 4);
	swd_ll_writel(&swd, SWD_AP_CTRL_WR, val);



	unsigned address = 0xe0042000;
//	unsigned step = 0x1000;
	swd_ll_writel(&swd, SWD_SELECT, 0x2);

//	unsigned address = 0xE000ED00;//0x410CC200;//0xE0042000;
	while(1) {
		ret = swd_ll_writel(&swd, SWD_AP_ADDR_WR, address);
//		swd_ll_writel(&swd, SWD_AP_ADDR_WR, 0x40015800);
		ret = swd_ll_writel(&swd, SWD_HEADER(1, 1, 2), 0xE0042000);
		ret = swd_ll_readl(&swd, SWD_AP_DATA_RD, &val);

		printf("0xf8 :%08x\n", val);
//		ret = swd_ll_writel(&swd, SWD_AP_DATA_WR, 0xA05F0003);
		ret = swd_ll_writel(&swd, SWD_AP_DATA_WR, address);

		ret = swd_ll_readl(&swd, SWD_AP_DATA_RD, &val);
		printf("%08x :%08x\n", address, val);
		address += 4;
		ret = swd_ll_readl(&swd, SWD_AP_CTRL_RD, &val);
		printf("SWD_AP_CTRL_RD :%08x\n", val);

		vTaskDelay(1000 / portTICK_PERIOD_MS);
	};

}
