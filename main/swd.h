#ifndef __SWD_H__
#define __SWD_H__

#include "swd_ll.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

extern int swd_ll_init(unsigned clk_speed, int mosi, int clk, int nrst);

struct swd_if {
	struct swd_dev *swd;
};

void swd_connect(void);
void swd_do_stuff(void);


#endif
